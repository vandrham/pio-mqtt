# pio-mqtt

platformio.ini:

  * lib_deps = git@bitbucket.org:vandrham/pio-mqtt.git

```
#include <MQTT.h>

WiFiBase wifi("ssid", "password");
MQTT mqtt(&wifi, "server.domain", "client_id", "mqtt_username", "mqtt_password");

void setup()
{
	mqtt.setup();
	mqtt.subscribe("topic/subtopic", [](String message) -> void { doSomethingWithMessage(message); });
}

void loop()
{
	mqtt.loop();
}

```