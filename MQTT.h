#ifndef mqtt_h
#define mqtt_h

#include <WiFiBase.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>


class MQTT
{
    public:
        MQTT(WiFiBase *wifi, const char *server, const char *id, const char *username, const char *password);
        void setup();
        void loop();
        void subscribe(const char *topic, void (*callback)(String));
        void publish(const char* topic, const char* payload);
    private:
        WiFiBase *_wifi;
        const char *_server;
        const char *_id;
        const char *_username;
        const char *_password;
        void connect();
        bool connect(int timeout);
        void resubscribe();

        struct Mapping
        {
            String topic;
            void (*callback)(String);
        };

        struct CallbackDictionary
        {
            Mapping* mappings;
            unsigned int capacity;
            unsigned int used;
        };

        CallbackDictionary* _callbacks;
};

#endif