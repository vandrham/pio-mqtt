#include "MQTT.h"
#include <WiFiBase.h>
#include <Arduino.h>

WiFiClient _wifiClient; 
PubSubClient _pubSubClient(_wifiClient);

MQTT::MQTT(WiFiBase *wifi, const char *server, const char *id, const char *username, const char *password)
{
    _wifi = wifi;
    _server = server;
    _id = id;
    _username = username;
    _password = password;
}

bool _test = true;

void MQTT::setup()
{
    _wifi -> connect();
    _pubSubClient.setServer(_server, 1883);

    /*
    _callbacks = (CallbackDictionary *)malloc(sizeof(CallbackDictionary));
    _callbacks -> capacity = 0;
    _callbacks -> used = 0;
    _callbacks -> mappings = (Mapping*)malloc(_callbacks -> capacity * sizeof(Mapping));
    memset(_callbacks -> mappings, 0, _callbacks -> capacity * sizeof(Mapping));

    _pubSubClient.setServer(_server, 1883);

    auto _cbs = _callbacks;
    _pubSubClient.setCallback([_cbs](char *topic, byte *payload, unsigned int length)
    {
        digitalWrite(LED_BUILTIN, _test ? LOW : HIGH);
        _test = !_test;

        auto topicString = String(topic);
        for (int i=0; i<_cbs -> used; i++)
        {
            auto mapping = &(_cbs -> mappings[i]);
            if (topicString.equals(mapping -> topic))
            {
                char arr[length + 1];
                for (int i = 0; i < length; i++) arr[i] = (char)payload[i];
                arr[length] = '\0';
                mapping -> callback(String(arr));
            }
        }
    });
    connect();
    */
}

void MQTT::subscribe(const char *topic, void (*callback)(String))
{
    if (_callbacks -> capacity == _callbacks -> used)
    {
        int newcapacity = _callbacks -> capacity + 1;
        auto mappings = (Mapping*)malloc(newcapacity * sizeof(Mapping));
        memset(mappings, 0, newcapacity * sizeof(Mapping));
        for (int i=0; i<_callbacks -> used; i++)
        {
            mappings[i] = _callbacks -> mappings[i];
        }
    
        if (_callbacks -> capacity > 0) free(_callbacks -> mappings);

        auto mapping = &(mappings[_callbacks -> used]);
        mapping -> topic = topic;
        mapping -> callback = callback;

        _callbacks -> capacity++;
        _callbacks -> used++;
        _callbacks -> mappings = mappings;
    }
    else
    {
        auto mapping = &(_callbacks -> mappings[_callbacks -> used]);
        mapping -> topic = topic;
        mapping -> callback = callback;
        _callbacks -> used++;
    }
    _pubSubClient.subscribe(topic);
    _pubSubClient.loop();
}

void MQTT::resubscribe()
{
    for (int i=0; i<_callbacks -> used; i++)
    {
        auto mapping = &(_callbacks -> mappings[i]);
        _pubSubClient.subscribe(mapping -> topic.c_str());
    }
}

void MQTT::connect()
{
    connect(-1);
}

bool MQTT::connect(int timeout)
{
    while (!_pubSubClient.connected())
    {
        if (!_pubSubClient.connect(_id, _username, _password))
        {
            delay(1000);
            if (--timeout == 0) return false;
        }
        
        resubscribe();
    }

    return true;
}

void MQTT::loop()
{
    if (!_pubSubClient.connected()) {
	_pubSubClient.connect(_id, _username, _password);
    }
    
    if (_pubSubClient.connected()) {
    	_pubSubClient.loop();
    }

    /*
    if (_wifi -> connect(5) && connect(5))
    {
        _pubSubClient.loop();
    }
    */
}

void MQTT::publish(const char* topic, const char* payload)
{
    if (_pubSubClient.connected()) {
        _pubSubClient.publish(topic, payload, false);
    }
}